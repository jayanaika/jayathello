package com.jayathello.jayathello.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jayathello.jayathello.activities.extras.GameActivityExtras;
import com.jayathello.jayathello.controllers.PlayerController;
import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.GameController;
import com.jayathello.jayathello.enums.GameType;
import com.jayathello.jayathello.views.GameView;

public class GameActivity extends AppCompatActivity {

    private PlayerController playerController;
    private GameView gameView;
    private GameController gameController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        GameActivityExtras gameActivityExtras = new GameActivityExtras(getIntent());

        playerController = new PlayerController(gameActivityExtras.getPlayer1(), gameActivityExtras.getPlayer2());

        gameView = new GameView(findViewById(R.id.activity_game),
                playerController.getCurrent());

        switch (gameActivityExtras.getGameType()) {
            case SINGLEPLAYER: {
                gameController = new GameController(this, playerController, gameActivityExtras.getDifficultyType());
            } break;

            case MULTIPLAYER: {
                gameController = new GameController(this, playerController);
            } break;
        }

        gameView.setController(gameController);
        gameController.setView(gameView);
        gameController.startGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameController.resumeGame();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameController.stopGame();
    }
}
