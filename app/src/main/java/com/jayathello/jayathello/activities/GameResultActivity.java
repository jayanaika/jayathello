package com.jayathello.jayathello.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.activities.extras.GameResultActivityExtras;
import com.jayathello.jayathello.controllers.GameResultController;
import com.jayathello.jayathello.views.GameResultView;

public class GameResultActivity extends AppCompatActivity {

    private GameResultView gameResultView;
    private GameResultController gameResultController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_result);

        GameResultActivityExtras gameResultActivityExtras = new GameResultActivityExtras(getIntent());

        gameResultView = new GameResultView(
                findViewById(R.id.activity_game_result),
                gameResultActivityExtras.getGameType(),
                gameResultActivityExtras.getPlayer1(),
                gameResultActivityExtras.getPlayer2());
        gameResultController = new GameResultController(this);

        gameResultView.setController(gameResultController);
        gameResultController.setView(gameResultView);
    }
}
