package com.jayathello.jayathello.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.LogInController;
import com.jayathello.jayathello.views.LogInView;

public class LogInActivity extends AppCompatActivity{

    private LogInView logInView;
    private LogInController loginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        logInView = new LogInView(findViewById(R.id.login));
        loginController = new LogInController(this);

        logInView.setController(loginController);
        loginController.setView(logInView);
    }

    @Override
    public void onPause() {
        super.onPause();
        loginController.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        loginController.onResume();
    }
}
