package com.jayathello.jayathello.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.MainMenuController;
import com.jayathello.jayathello.views.MainMenuView;

public class MainMenuActivity extends AppCompatActivity {

    private MainMenuView mainMenuView;
    private MainMenuController mainMenuController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        mainMenuView = new MainMenuView(findViewById(R.id.mainMenu));
        mainMenuController = new MainMenuController(this);

        mainMenuView.setController(mainMenuController);
        mainMenuController.setView(mainMenuView);
    }
}
