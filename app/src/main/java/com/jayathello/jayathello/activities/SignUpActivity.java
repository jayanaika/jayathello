package com.jayathello.jayathello.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.SignUpController;
import com.jayathello.jayathello.views.SignUpView;

public class SignUpActivity extends AppCompatActivity {

    private SignUpView signUpView;
    private SignUpController signUpController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signUpView = new SignUpView(findViewById(R.id.signup));
        signUpController = new SignUpController(this);

        signUpView.setController(signUpController);
        signUpController.setView(signUpView);
    }

    @Override
    public void onPause() {
        super.onPause();
        signUpController.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        signUpController.onResume();
    }
}
