package com.jayathello.jayathello.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.canvas.StoneSymbol;
import com.jayathello.jayathello.controllers.SinglePlayerOptionsController;
import com.jayathello.jayathello.enums.StoneType;
import com.jayathello.jayathello.views.SinglePlayerOptionsView;

public class SinglePlayerOptionsActivity extends AppCompatActivity {

    private SinglePlayerOptionsView singlePlayerOptionsView;
    private SinglePlayerOptionsController singlePlayerOptionsController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player_options);

        singlePlayerOptionsView = new SinglePlayerOptionsView(findViewById(R.id.activity_single_player_options));
        singlePlayerOptionsController = new SinglePlayerOptionsController(this);

        singlePlayerOptionsView.setController(singlePlayerOptionsController);
        singlePlayerOptionsController.setView(singlePlayerOptionsView);
    }
}
