package com.jayathello.jayathello.activities.extras;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Julian on 05.02.2017.
 */

public abstract class ActivityExtras implements Serializable {
    protected static final String EXTRAS_STRING = "Extras";
    protected HashMap<String, Serializable> extras;

    protected ActivityExtras() {
        extras = new HashMap<>();
    }

    protected ActivityExtras(HashMap<String, Serializable> extras) {
        this.extras = extras;
    }

    public HashMap<String, Serializable> getExtras() {
        return extras;
    }

    public String getExtrasString() {
        return EXTRAS_STRING;
    }

    public abstract Class getActivity();
}
