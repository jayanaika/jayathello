package com.jayathello.jayathello.activities.extras;

import android.content.Intent;

import com.jayathello.jayathello.entity.Player;
import com.jayathello.jayathello.activities.GameResultActivity;
import com.jayathello.jayathello.enums.GameType;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Julian on 05.02.2017.
 */

public class GameResultActivityExtras extends ActivityExtras {
    private static final String GAME_TYPE = "GameType";
    private static final String PLAYER1 = "Player1";
    private static final String PLAYER2 = "Player2";

    private static final Class activity = GameResultActivity.class;

    public GameResultActivityExtras(Player player1, Player player2) {
        extras.put(PLAYER1, player1);
        extras.put(PLAYER2, player2);
    }

    public GameResultActivityExtras(Intent intent) {
        super((HashMap<String, Serializable>) intent.getSerializableExtra(EXTRAS_STRING));
    }

    public GameType getGameType() {
        return (GameType) extras.get(GAME_TYPE);
    }

    public Player getPlayer1() {
        return (Player) extras.get(PLAYER1);
    }

    public Player getPlayer2() {
        return (Player) extras.get(PLAYER2);
    }

    @Override
    public Class getActivity() {
        return activity;
    }
}
