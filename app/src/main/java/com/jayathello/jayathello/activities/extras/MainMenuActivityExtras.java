package com.jayathello.jayathello.activities.extras;

import android.content.Intent;

import com.jayathello.jayathello.activities.MainMenuActivity;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Julian on 05.02.2017.
 */

public class MainMenuActivityExtras extends ActivityExtras {
    private static final Class activity = MainMenuActivity.class;

    public MainMenuActivityExtras() {

    }

    public MainMenuActivityExtras(Intent intent) {
        super((HashMap<String, Serializable>) intent.getSerializableExtra(EXTRAS_STRING));
    }

    @Override
    public Class getActivity() {
        return activity;
    }
}
