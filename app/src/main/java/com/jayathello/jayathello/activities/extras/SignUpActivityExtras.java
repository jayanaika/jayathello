package com.jayathello.jayathello.activities.extras;

import android.content.Intent;

import com.jayathello.jayathello.activities.SignUpActivity;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Julian on 12.02.2017.
 */

public class SignUpActivityExtras extends ActivityExtras {
    private static final Class activity = SignUpActivity.class;

    public SignUpActivityExtras() {

    }

    public SignUpActivityExtras(Intent intent) {
        super((HashMap<String, Serializable>) intent.getSerializableExtra(EXTRAS_STRING));
    }

    @Override
    public Class getActivity() {
        return activity;
    }
}
