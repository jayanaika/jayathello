package com.jayathello.jayathello.activities.extras;

import android.content.Intent;

import com.jayathello.jayathello.activities.SinglePlayerOptionsActivity;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Julian on 13.02.2017.
 */

public class SinglePlayerOptionsActivityExtras extends ActivityExtras {
    private static final Class activity = SinglePlayerOptionsActivity.class;

    public SinglePlayerOptionsActivityExtras() {

    }

    public SinglePlayerOptionsActivityExtras(Intent intent) {
        super((HashMap<String, Serializable>) intent.getSerializableExtra(EXTRAS_STRING));
    }

    @Override
    public Class getActivity() {
        return activity;
    }
}
