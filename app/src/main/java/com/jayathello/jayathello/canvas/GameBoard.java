package com.jayathello.jayathello.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.entity.Player;
import com.jayathello.jayathello.util.Board;

/**
 * Created by julian on 26.10.16.
 */

public class GameBoard extends View {

    private static final int BOARD_SIZE = Board.getBoardSize();

    private Paint paint;
    private float squareSize;

    private Player currentPlayer;

    public GameBoard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeJoin(Paint.Join.BEVEL);
        paint.setStrokeWidth(4.F);

        setVisibility(INVISIBLE);
    }

    public void init(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
        setVisibility(VISIBLE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBoard(canvas);
        drawStones(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        squareSize = getWidth() / BOARD_SIZE;
    }

    private void drawBoard(Canvas canvas) {
        for (int y = 0; y < BOARD_SIZE; y++) {
            for (int x = 0; x < BOARD_SIZE; x++) {
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(getResources().getColor(R.color.colorJayaGreen));
                canvas.drawRect(new RectF(x * squareSize, y * squareSize, x * squareSize + squareSize, y * squareSize + squareSize), paint);

                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(Color.BLACK);
                canvas.drawRect(new RectF(x * squareSize, y * squareSize, x * squareSize + squareSize, y * squareSize + squareSize), paint);
            }
        }
    }

    private void drawStones(Canvas canvas) {
        paint.setStyle(Paint.Style.FILL);

        paint.setColor(currentPlayer.getStoneType().getTurnColor());
        drawStonesFor(canvas, currentPlayer.getTurns(), 5);

        paint.setColor(currentPlayer.getStoneType().getColor());
        drawStonesFor(canvas, currentPlayer.getStones(), 1);

        paint.setColor(currentPlayer.getOpponent().getStoneType().getColor());
        drawStonesFor(canvas, currentPlayer.getOpponent().getStones(), 1);
    }

    private void drawStonesFor(Canvas canvas, long bitBoard, float paddingMultiplier) {
        float padding = 5.F;
        float r = squareSize / 2.F;

        while (bitBoard != 0L) {
            long stone = bitBoard ^ (bitBoard & (bitBoard - 1));
            int i = Long.numberOfTrailingZeros(stone);
            int x = i % BOARD_SIZE;
            int y = i / BOARD_SIZE;

            canvas.drawCircle(x * squareSize + r, y * squareSize + r, r - padding * paddingMultiplier, paint);

            bitBoard &= bitBoard - 1;
        }
    }

    public float getSquareSize() {
        return squareSize;
    }

    public void render(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
        postInvalidate();
    }
}

