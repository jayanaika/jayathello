package com.jayathello.jayathello.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.jayathello.jayathello.enums.StoneType;

/**
 * Created by julian on 22.11.16.
 */

//Text inside circle
//http://stackoverflow.com/questions/22224384/draw-text-inside-android-circle 09.02.2017
public class StoneSymbol extends View {
    private StoneType stoneType;
    private Paint circlePaint;
    private Paint textPaint;
    private Rect bounds;
    private String text;

    //color inversion
    //http://stackoverflow.com/questions/17841787/invert-colors-of-drawable-android 09.02.2017
    private static final float[] NEGATIVE = {
            -1.0f,     0,       0,     0, 255, // red
            0,      -1.0f,      0,     0, 255, // green
            0,         0,    -1.0f,    0, 255, // blue
            0,         0,       0,   1.0f,  0  // alpha
    };

    public StoneSymbol(Context context, AttributeSet attrs) {
        super(context, attrs);

        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setStyle(Paint.Style.FILL);

        textPaint = new Paint();
        textPaint.setTextSize(64.F);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setColorFilter(new ColorMatrixColorFilter(NEGATIVE));

        bounds = new Rect();

        text = "";

        setVisibility(INVISIBLE);
    }

    public void init(StoneType stoneType) {
        this.stoneType = stoneType;
        circlePaint.setColor(stoneType.getColor());
        textPaint.setColor(stoneType.getColor());
        setVisibility(VISIBLE);
    }

    public void init(StoneType stoneType, String text) {
        this.stoneType = stoneType;
        circlePaint.setColor(stoneType.getColor());
        textPaint.setColor(stoneType.getColor());
        this.text = text;
        setVisibility(VISIBLE);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        postInvalidate();
    }

    public StoneType getStoneType() {
        return stoneType;
    }

    public void setStoneType(StoneType stoneType) {
        this.stoneType = stoneType;
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float w = getWidth() / 2.F;
        float h = getHeight() / 2.F;
        float r = Math.min(w, h);

        textPaint.getTextBounds(text, 0, text.length(), bounds);

        canvas.drawCircle(w, h , r / 1.5F, circlePaint);
        canvas.drawText(text, w, h + (bounds.height() / 2), textPaint);
    }

}
