package com.jayathello.jayathello.controllers;

import android.app.Activity;
import android.util.Pair;

import com.jayathello.jayathello.enums.DifficultyType;
import com.jayathello.jayathello.util.Board;
import com.jayathello.jayathello.util.GameTree;
import com.jayathello.jayathello.activities.extras.GameResultActivityExtras;
import com.jayathello.jayathello.controllers.interfaces.GameInterface;
import com.jayathello.jayathello.enums.PlayerType;
import com.jayathello.jayathello.views.GameView;

import java.util.concurrent.SynchronousQueue;

/**
 * Created by Julian on 29.10.2016.
 */

public class GameController extends JayaController<GameView> implements GameInterface {

    private Thread gameLoop;

    private SynchronousQueue<Long> touchEvents;

    private PlayerController playerController;

    private DifficultyType difficultyType;

    private boolean gameOver;
    private boolean gameStopped;

    public GameController(Activity activity, PlayerController playerController) {
        super(activity);
        this.playerController = playerController;
        Board.initBitBoards(playerController.getPlayer1(), playerController.getPlayer2());
        touchEvents = new SynchronousQueue<>();
        gameOver = false;
        gameStopped = false;
        playerController.getCurrent().setTurns(Board.calculatePossibleTurns(
                playerController.getCurrent().getStones(),
                playerController.getCurrent().getOpponent().getStones()));
    }

    public GameController(Activity activity, PlayerController playerController, DifficultyType difficultyType) {
        this(activity, playerController);
        this.difficultyType = difficultyType;
    }

    private void doTurn(long turn) {
        if (turn != 0) {
            Pair<Long, Long> boardPair = Board.flipStones(
                    playerController.getCurrent().getStones(),
                    playerController.getCurrent().getOpponent().getStones(), turn);

            playerController.getCurrent().setStones(boardPair.first);
            playerController.getCurrent().getOpponent().setStones(boardPair.second);
            nextPlayer(2);
        }
    }

    private void nextPlayer(int numberOfPlayers) {
        if (numberOfPlayers == 0) {
            gameOver = true;
            return;
        }

        playerController.nextPlayer();
        playerController.getCurrent().setTurns(Board.calculatePossibleTurns(
                playerController.getCurrent().getStones(),
                playerController.getCurrent().getOpponent().getStones()));

        if(playerController.getCurrent().getTurns() == 0) {
            nextPlayer(numberOfPlayers-1);
        }
    }

    @Override
    public void onTouchDown(int x, int y) {
        touchEvents.offer(Board.calculateAndValidateTurn(playerController.getCurrent().getTurns(), x, y));
    }

    public void startGame() {
        gameLoop = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    runGame();
                }

                catch (InterruptedException e) {
                    gameStopped  = true;
                }
            }
        });
        gameLoop.start();
    }

    public void resumeGame() {
        gameStopped = false;
        if (!gameLoop.isAlive()) {
            startGame();
        }
    }

    public void stopGame() {
        gameStopped = true;
        if (!gameLoop.isInterrupted()) {
            gameLoop.interrupt();
        }
    }

    private synchronized void runGame() throws InterruptedException {
        while (!gameOver && !gameStopped) {
            long turn;
            if (playerController.getCurrent().getType() == PlayerType.PLAYER) {
                turn = getUserInput();
            }

            else {
                turn = GameTree.getTurn(playerController.getCurrent(), difficultyType.getDepth());
                wait(500L);
            }
            doTurn(turn);
            render();
        }

        if (gameOver) {
            gameOver();
        }
    }

    private long getUserInput() throws InterruptedException {
        Long l = touchEvents.take();

        if (l == null) {
            return 0L;
        }

        return l;
    }

    private void render() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.updateGameBoard(playerController.getCurrent());
                view.updateNumberOfStones(Board.countSetBits(playerController.getPlayer1().getStones()),
                        Board.countSetBits(playerController.getPlayer2().getStones()));
            }
        });
    }

    private void gameOver() {
        GameResultActivityExtras gameResultActivityExtras = new GameResultActivityExtras(activity.getIntent());

        changeActivity(gameResultActivityExtras);
        activity.finish();
    }
}
