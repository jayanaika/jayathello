package com.jayathello.jayathello.controllers;



import android.app.Activity;

import com.jayathello.jayathello.activities.extras.GameActivityExtras;
import com.jayathello.jayathello.controllers.interfaces.GameResultInterface;
import com.jayathello.jayathello.views.GameResultView;

/**
 * Created by julian on 02.02.17.
 */

public class GameResultController extends JayaController<GameResultView> implements GameResultInterface {

    public GameResultController(Activity activity) {
        super(activity);
    }

    @Override
    public void rematch() {
        GameActivityExtras gameActivityExtras = new GameActivityExtras(activity.getIntent());
        changeActivity(gameActivityExtras);
        activity.finish();
    }

    @Override
    public void backToMenu() {
        activity.finish();
    }
}
