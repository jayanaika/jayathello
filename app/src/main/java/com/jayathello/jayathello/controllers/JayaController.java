package com.jayathello.jayathello.controllers;

import android.app.Activity;
import android.content.Intent;

import com.jayathello.jayathello.activities.extras.ActivityExtras;
import com.jayathello.jayathello.views.JayaView;

/**
 * Created by julian on 03.02.17.
 */

public abstract class JayaController<T extends JayaView> {
    protected Activity activity;
    protected T view;

    protected JayaController(Activity activity) {
        this.activity = activity;
    }

    protected void changeActivity(ActivityExtras activityExtras) {
        Intent intent = new Intent(activity, activityExtras.getActivity());
        intent.putExtra(activityExtras.getExtrasString(), activityExtras.getExtras());
        activity.startActivity(intent);
    }

    public void setView(T view) {
        this.view = view;
    }
}
