package com.jayathello.jayathello.controllers;

import android.app.Activity;

import com.jayathello.jayathello.database.DatabaseTable;
import com.jayathello.jayathello.database.dao.UserDAO;
import com.jayathello.jayathello.R;
import com.jayathello.jayathello.activities.extras.MainMenuActivityExtras;
import com.jayathello.jayathello.activities.extras.SignUpActivityExtras;
import com.jayathello.jayathello.database.entity.User;
import com.jayathello.jayathello.encryption.PasswordEncrypter;
import com.jayathello.jayathello.controllers.interfaces.LogInInterface;
import com.jayathello.jayathello.views.LogInView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by julian on 25.01.17.
 */

public class LogInController extends JayaController<LogInView> implements LogInInterface {
    private static final Pattern ALLOWED_CHARACTERS = Pattern.compile("^$|[^A-Za-z0-9_.@-]+");
    private UserDAO userDAO;

    public LogInController(Activity activity) {
        super(activity);
        userDAO = new UserDAO(activity);
    }

    @Override
    public void logIn(String email, String password) {
        if (isValidEmail(email) && isValidPassword(password)) {
            Map<DatabaseTable.User.Fields, String> whereMap = new HashMap<>();
            whereMap.put(DatabaseTable.User.Fields.EMAIL, email);

            User user = userDAO.selectOne(whereMap);

            if (user != null) {
                String passwordHash = PasswordEncrypter.generatePasswordHash(
                        password, user.getPassword().getSalt(), user.getPassword().getIterations());

                if (user.getPassword().getHash().equals(passwordHash)) {
                    view.clearTextInput();
                    userDAO.close();
                    changeActivity(new MainMenuActivityExtras());
                }

                else {
                    view.setEmailError(activity.getResources().getString(R.string.loginFailed));
                }
            }

            else {
                view.setEmailError(activity.getResources().getString(R.string.loginFailed));
            }
        }
    }

    @Override
    public void signUp() {
        view.clearTextInput();
        changeActivity(new SignUpActivityExtras());
    }

    @Override
    public boolean isValidEmail(String email) {
        Matcher matcher = ALLOWED_CHARACTERS.matcher(email);

        if (matcher.find()) {
            view.setEmailError(activity.getResources().getString(R.string.invalidCharacters));
            return false;
        }

        view.setEmailError("");
        return true;
    }

    @Override
    public boolean isValidPassword(String password) {
        Matcher matcher = ALLOWED_CHARACTERS.matcher(password);

        if (matcher.find()) {
            view.setPasswordError(activity.getResources().getString(R.string.invalidCharacters));
            return false;
        }

        view.setPasswordError("");
        return true;
    }

    public void onPause() {
        userDAO.close();
    }

    public void onResume() {
        userDAO.open();
    }
}
