package com.jayathello.jayathello.controllers;

import android.app.Activity;

import com.jayathello.jayathello.entity.Player;
import com.jayathello.jayathello.activities.extras.GameActivityExtras;
import com.jayathello.jayathello.activities.extras.SinglePlayerOptionsActivityExtras;
import com.jayathello.jayathello.enums.PlayerType;
import com.jayathello.jayathello.enums.StoneType;
import com.jayathello.jayathello.controllers.interfaces.MainMenuInterface;
import com.jayathello.jayathello.views.MainMenuView;

/**
 * Created by julian on 29.01.17.
 */

public class MainMenuController extends JayaController<MainMenuView> implements MainMenuInterface {

    public MainMenuController(Activity activity) {
        super(activity);
    }

    @Override
    public void singlePlayer() {
        changeActivity(new SinglePlayerOptionsActivityExtras());
    }

    @Override
    public void multiPlayer() {
        Player player1 = new Player(StoneType.BLACK, PlayerType.PLAYER);
        Player player2 = new Player(StoneType.WHITE, PlayerType.PLAYER);

        GameActivityExtras gameActivityExtras = new GameActivityExtras(player1, player2);

        changeActivity(gameActivityExtras);
    }
}
