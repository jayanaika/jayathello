package com.jayathello.jayathello.controllers;

import com.jayathello.jayathello.entity.Player;

/**
 * Created by Julian on 29.10.2016.
 */

public class PlayerController {
    private Player player1;
    private Player player2;
    private Player currentPlayer;

    public PlayerController(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.player1.setOpponent(this.player2);
        this.player2.setOpponent(this.player1);
        currentPlayer = player1;
    }

    public void nextPlayer() {
        currentPlayer = currentPlayer.getOpponent();
    }

    public Player getPlayer1() { return player1; }

    public Player getPlayer2() { return  player2; }

    public Player getCurrent() {
        return currentPlayer;
    }
}


