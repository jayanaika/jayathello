package com.jayathello.jayathello.controllers;

import android.app.Activity;

import com.jayathello.jayathello.database.DatabaseTable;
import com.jayathello.jayathello.database.dao.UserDAO;
import com.jayathello.jayathello.R;
import com.jayathello.jayathello.activities.extras.MainMenuActivityExtras;
import com.jayathello.jayathello.database.entity.User;
import com.jayathello.jayathello.encryption.Password;
import com.jayathello.jayathello.encryption.PasswordEncrypter;
import com.jayathello.jayathello.controllers.interfaces.SignUpInterface;
import com.jayathello.jayathello.views.SignUpView;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Julian on 12.02.2017.
 */

public class SignUpController extends JayaController<SignUpView> implements SignUpInterface {
    private static final int PASSWORD_LENGTH = 8;
    private static final Pattern ALLOWED_CHARACTERS = Pattern.compile("^$|[^A-Za-z0-9_.@-]+");
    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    private UserDAO userDAO;

    public SignUpController(Activity activity) {
        super(activity);
        userDAO = new UserDAO(activity);
    }

    private boolean containsUnallowedCharacters(String text) {
        Matcher matcher = ALLOWED_CHARACTERS.matcher(text);
        return matcher.find();
    }

    @Override
    public void signUp(String email, String password, String confirmPassword) {
        if (isEmail(email) && isEmailAvailable(email) &&
                isPasswordLongEnough(password) && isPasswordSame(password, confirmPassword)) {
            Password pwd = PasswordEncrypter.generatePassword(password);

            Map<DatabaseTable.User.Fields, String> whereMap = new HashMap<>();
            whereMap.put(DatabaseTable.User.Fields.EMAIL, email);

            User user = userDAO.selectOne(whereMap);

            if (user == null) {
                user = new User();
                user.setEmail(email);
                user.setPassword(pwd);
                userDAO.insert(user);
                userDAO.close();

                changeActivity(new MainMenuActivityExtras());
                activity.finish();
            }
        }
    }

    @Override
    public boolean isEmail(String email) {
        if (containsUnallowedCharacters(email)) {
            view.setEmailError(activity.getResources().getString(R.string.invalidCharacters));
            return false;
        }

        Matcher matcher = EMAIL_PATTERN.matcher(email);
        if (!matcher.find()) {
            view.setEmailError(activity.getResources().getString(R.string.invalidEmail));
            return false;
        }

        view.setEmailError("");
        return true;
    }

    @Override
    public boolean isEmailAvailable(String email) {
        Map<DatabaseTable.User.Fields, String> whereMap = new HashMap<>();
        whereMap.put(DatabaseTable.User.Fields.EMAIL, email);

        User user = userDAO.selectOne(whereMap);

        if (user != null) {
            view.setEmailError(activity.getResources().getString(R.string.userAlreadyExists));
            return false;
        }
        view.setEmailError("");
        return true;
    }

    @Override
    public boolean isPasswordLongEnough(String password) {
        if (containsUnallowedCharacters(password)) {
            view.setPasswordError(activity.getResources().getString(R.string.invalidCharacters));
            return false;
        }

        if (password.length() < PASSWORD_LENGTH) {
            view.setPasswordError(
                    activity.getResources().getString(R.string.passwordTooShortFirst) + ' ' +
                    PASSWORD_LENGTH + ' ' +
                    activity.getResources().getString(R.string.passwordTooShortSecond));
            return false;
        }
        view.setPasswordError("");
        return true;
    }

    @Override
    public boolean isPasswordSame(String password, String confirmPassword) {
        if (!password.equals(confirmPassword)) {
            view.setConfirmPasswordError(activity.getResources().getString(R.string.passwordsDoNotMatch));
            return false;
        }
        view.setConfirmPasswordError("");
        return true;
    }

    public void onPause() {
        userDAO.close();
    }

    public void onResume() {
        userDAO.open();
    }
}
