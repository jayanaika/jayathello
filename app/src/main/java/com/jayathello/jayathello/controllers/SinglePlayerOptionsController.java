package com.jayathello.jayathello.controllers;

import android.app.Activity;

import com.jayathello.jayathello.activities.extras.GameActivityExtras;
import com.jayathello.jayathello.controllers.interfaces.SinglePlayerOptionsInterface;
import com.jayathello.jayathello.entity.Player;
import com.jayathello.jayathello.enums.DifficultyType;
import com.jayathello.jayathello.enums.PlayerType;
import com.jayathello.jayathello.enums.StoneType;
import com.jayathello.jayathello.views.SinglePlayerOptionsView;

/**
 * Created by Julian on 25.02.2017.
 */

public class SinglePlayerOptionsController extends JayaController<SinglePlayerOptionsView> implements SinglePlayerOptionsInterface {
    public SinglePlayerOptionsController(Activity activity) {
        super(activity);
    }

    @Override
    public void startGame(DifficultyType difficultyType, StoneType stoneType) {
        Player player1;
        Player player2;

        if (stoneType == StoneType.BLACK) {
            player1 = new Player(StoneType.BLACK, PlayerType.PLAYER);
            player2 = new Player(StoneType.WHITE, PlayerType.AI);
        }

        else {
            player1 = new Player(StoneType.BLACK, PlayerType.AI);
            player2 = new Player(StoneType.WHITE, PlayerType.PLAYER);
        }

        GameActivityExtras gameActivityExtras = new GameActivityExtras(player1, player2, difficultyType);

        changeActivity(gameActivityExtras);
    }
}
