package com.jayathello.jayathello.controllers.interfaces;

/**
 * Created by Julian on 29.10.2016.
 */

public interface GameInterface extends JayaControllerInterface {
    void onTouchDown(int x, int y);
}
