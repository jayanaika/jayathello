package com.jayathello.jayathello.controllers.interfaces;

/**
 * Created by Julian on 04.02.2017.
 */

public interface GameResultInterface extends JayaControllerInterface {
    void rematch();
    void backToMenu();
}
