package com.jayathello.jayathello.controllers.interfaces;

/**
 * Created by Julian on 04.02.2017.
 */

public interface LogInInterface extends JayaControllerInterface {
    void logIn(String email, String password);
    void signUp();
    boolean isValidEmail(String email);
    boolean isValidPassword(String password);
}
