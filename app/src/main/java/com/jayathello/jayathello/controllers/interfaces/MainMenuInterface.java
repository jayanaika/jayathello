package com.jayathello.jayathello.controllers.interfaces;

/**
 * Created by Julian on 04.02.2017.
 */

public interface MainMenuInterface extends JayaControllerInterface {
    void singlePlayer();
    void multiPlayer();
}
