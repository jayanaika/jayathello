package com.jayathello.jayathello.controllers.interfaces;

/**
 * Created by Julian on 12.02.2017.
 */

public interface SignUpInterface extends JayaControllerInterface {
    void signUp(String email, String password, String confirmPassword);
    boolean isEmail(String email);
    boolean isEmailAvailable(String email);
    boolean isPasswordLongEnough(String password);
    boolean isPasswordSame(String password, String confirmPassword);
}
