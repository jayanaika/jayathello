package com.jayathello.jayathello.controllers.interfaces;

import com.jayathello.jayathello.enums.DifficultyType;
import com.jayathello.jayathello.enums.StoneType;

/**
 * Created by Julian on 25.02.2017.
 */

public interface SinglePlayerOptionsInterface extends JayaControllerInterface {
    void startGame(DifficultyType difficultyType, StoneType stoneType);
}
