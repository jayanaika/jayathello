package com.jayathello.jayathello.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Julian on 15.02.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int databaseVersion = 1;
    private static final String databaseName = "jayathelloDatabase";

    private static DatabaseHelper databaseHelper;

    private DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

    public static DatabaseHelper getHelper(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseTable.User.getCreateString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DatabaseTable.User.getDropString());
        this.onCreate(db);
    }
}
