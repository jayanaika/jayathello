package com.jayathello.jayathello.database;

/**
 * Created by Julian on 17.02.2017.
 */

public class DatabaseTable {
    private DatabaseTable() {}

    public static class User {
        private static final String name = "userTable";

        public enum Fields {
            ID("id"),
            EMAIL("email"),
            PASSWORD("password"),
            SALT("salt"),
            ITERATIONS("iterations");

            private String name;

            Fields(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return name;
            }
        }

        private User() {}

        public static String getName() {
            return name;
        }

        public static String getCreateString() {
            return String.format("CREATE TABLE IF NOT EXISTS %s (" +
                    "%s INT PRIMARY KEY," +
                    " %s VARCHAR NOT NULL," +
                    " %s VARCHAR NOT NULL," +
                    " %s VARCHAR NOT NULL," +
                    " %s INT NOT NULL);",
                    name, Fields.ID, Fields.EMAIL, Fields.PASSWORD, Fields.SALT, Fields.ITERATIONS);
        }

        public static String getDropString() {
            return String.format("DROP TABLE IF EXISTS %s", name);
        }
    }
}