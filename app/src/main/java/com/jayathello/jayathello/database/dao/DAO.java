package com.jayathello.jayathello.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.jayathello.jayathello.database.DatabaseHelper;

import java.util.List;
import java.util.Map;

/**
 * Created by Julian on 15.02.2017.
 */

/**
 *
 * @param <E> The database entity
 * @param <F> The database table fields enum
 */
public abstract class DAO<E, F extends Enum> {
    protected DatabaseHelper databaseHelper;
    protected SQLiteDatabase database;
    protected Context context;

    protected DAO (Context context) {
        this.context = context;
        this.databaseHelper = DatabaseHelper.getHelper(context);
        open();
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();
        database = null;
    }

    public abstract E selectOne(Map<F, String> whereMap);

    public abstract List<E> selectAll();
    public abstract List<E> selectAll(Map<F, String> whereMap);

    public abstract void insert(E entity);

    public void insert(Iterable<E> iterableEntity) {
        for (E entity : iterableEntity) {
            insert(entity);
        }
    }

    public abstract void update(E entity);

    public void update(Iterable<E> iterableEntity) {
        for (E entity : iterableEntity) {
            insert(entity);
        }
    }

    public abstract void delete(E entity);

    public void delete(Iterable<E> iterableEntity) {
        for (E entity : iterableEntity) {
            insert(entity);
        }
    }

    protected String buildWhereString(Map<F, String> whereMap) {
        StringBuilder where = new StringBuilder();

        for (Map.Entry<F, String> entry : whereMap.entrySet()) {
            where.append(String.format("%s=\"%s\" ", entry.getKey(), entry.getValue()));
        }

        return where.toString();
    }

    protected abstract ContentValues getContentValues(E entity);
}
