package com.jayathello.jayathello.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;

import com.jayathello.jayathello.database.DatabaseTable;
import com.jayathello.jayathello.database.entity.User;
import com.jayathello.jayathello.encryption.Password;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.jayathello.jayathello.database.DatabaseTable.User.Fields.EMAIL;
import static com.jayathello.jayathello.database.DatabaseTable.User.Fields.ID;
import static com.jayathello.jayathello.database.DatabaseTable.User.Fields.ITERATIONS;
import static com.jayathello.jayathello.database.DatabaseTable.User.Fields.PASSWORD;
import static com.jayathello.jayathello.database.DatabaseTable.User.Fields.SALT;


public class UserDAO extends DAO<User, DatabaseTable.User.Fields> {
    private static final String tableName = DatabaseTable.User.getName();

    public UserDAO(Context context) {
        super(context);
    }

    @Override
    @Nullable
    public User selectOne(Map<DatabaseTable.User.Fields, String> whereMap) {
        final String select = String.format("SELECT * FROM %s WHERE %s", tableName, buildWhereString(whereMap));
        Cursor c = database.rawQuery(select, null);
        return c.moveToNext() ? createUser(c) : null;
    }

    @Override
    public List<User> selectAll() {
        final String select = String.format("SELECT * FROM %s", tableName);
        Cursor c = database.rawQuery(select, null);
        return createUserList(c);
    }

    @Override
    public List<User> selectAll(Map<DatabaseTable.User.Fields, String> whereMap) {
        if (whereMap.isEmpty()) {
            return selectAll();
        }

        final String select = String.format("SELECT * FROM %s WHERE %s", tableName, buildWhereString(whereMap));
        Cursor c = database.rawQuery(select, null);
        return createUserList(c);
    }

    @Override
    public void insert(User entity) {
        database.insert(tableName, null, getContentValues(entity));
    }

    @Override
    public void update(User entity) {
        final String where = String.format("%s=%d", ID, entity.getId());
        database.update(tableName, getContentValues(entity), where, null);
    }

    @Override
    public void delete(User entity) {
        final String where = String.format("%s=%d", ID, entity.getId());
        database.delete(tableName, where, null);
    }

    @Override
    protected ContentValues getContentValues(User entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EMAIL.toString(), entity.getEmail());
        contentValues.put(PASSWORD.toString(), entity.getPassword().getHash());
        contentValues.put(SALT.toString(), entity.getPassword().getSalt());
        contentValues.put(ITERATIONS.toString(), entity.getPassword().getIterations());
        return contentValues;
    }

    private User createUser(Cursor c) {
        User user = new User();
        user.setId(c.getInt(c.getColumnIndex(ID.toString())));
        user.setEmail(c.getString(c.getColumnIndex(PASSWORD.toString())));
        user.setPassword(new Password(
                c.getString(c.getColumnIndex(PASSWORD.toString())),
                c.getString(c.getColumnIndex(SALT.toString())),
                c.getInt(c.getColumnIndex(ITERATIONS.toString()))
        ));
        return user;
    }

    private List<User> createUserList(Cursor c) {
        List<User> users = new ArrayList<>();

        while (c.moveToNext()) {
            users.add(createUser(c));
        }
        c.close();
        return users;
    }
}
