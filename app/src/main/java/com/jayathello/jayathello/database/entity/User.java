package com.jayathello.jayathello.database.entity;

import com.jayathello.jayathello.encryption.Password;

/**
 * Created by Julian on 14.02.2017.
 */

public class User {
    private int id;
    private String email;
    private Password password;

    public User() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
