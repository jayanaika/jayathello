package com.jayathello.jayathello.encryption;

/**
 * Created by julian on 02.02.17.
 */

public class Password {
    private String hash;
    private String salt;
    private int iterations;

    public Password(String hash, String salt, int iterations) {
        this.hash = hash;
        this.salt = salt;
        this.iterations = iterations;
    }

    public String getHash() {
        return hash;
    }

    public String getSalt() {
        return salt;
    }

    public int getIterations() {
        return iterations;
    }
}
