package com.jayathello.jayathello.entity;

import com.jayathello.jayathello.enums.PlayerType;
import com.jayathello.jayathello.enums.StoneType;

import java.io.Serializable;

/**
 * Created by Julian on 06.11.2016.
 */

public class Player implements Serializable {
    private StoneType stoneType;
    private PlayerType type;
    private Player opponent;

    private long stones;
    private long turns;

    public Player(StoneType stoneType, PlayerType type) {
        this.stoneType = stoneType;
        this.type = type;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

    public StoneType getStoneType() {
        return stoneType;
    }

    public PlayerType getType() {
        return type;
    }

    public Player getOpponent() {
        return opponent;
    }

    public long getStones() {

        return stones;
    }

    public void setStones(long stones) {
        this.stones = stones;
    }

    public long getTurns() {
        return turns;
    }

    public void setTurns(long turns) {
        this.turns = turns;
    }
}
