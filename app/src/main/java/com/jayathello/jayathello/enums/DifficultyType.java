package com.jayathello.jayathello.enums;

/**
 * Created by Julian on 25.02.2017.
 */

public enum DifficultyType {
    EASY(2),
    MEDIUM(4),
    HARD(6);

    private int depth;

    DifficultyType(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }
}
