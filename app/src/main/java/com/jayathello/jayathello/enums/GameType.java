package com.jayathello.jayathello.enums;

/**
 * Created by Julian on 25.02.2017.
 */

public enum GameType {
    SINGLEPLAYER,
    MULTIPLAYER;
}
