package com.jayathello.jayathello.enums;

/**
 * Created by Julian on 29.10.2016.
 */

public enum PlayerType {
    PLAYER,
    AI
}
