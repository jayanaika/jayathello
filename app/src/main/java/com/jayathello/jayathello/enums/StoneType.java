package com.jayathello.jayathello.enums;

import android.graphics.Color;

/**
 * Created by julian on 26.10.16.
 */

public enum StoneType {
    BLACK(Color.BLACK, Color.argb(64, 20, 20, 20)),
    WHITE(Color.WHITE, Color.argb(191, 235, 235, 235));

    private int color;
    private int turnColor;

    StoneType(int color, int turnColor) {
        this.color = color;
        this.turnColor = turnColor;
    }

    public int getColor() {
        return color;
    }

    public int getTurnColor() { return turnColor; }
}
