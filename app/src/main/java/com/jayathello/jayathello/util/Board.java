package com.jayathello.jayathello.util;

import android.util.Pair;

import com.jayathello.jayathello.entity.Player;

/**
 * Created by julian on 26.10.16.
 */

public class Board {
    private static final int BOARD_SIZE = 8;

    private static final long LEFT = 0x7F7F7F7F7F7F7F7FL;
    private static final long RIGHT = 0xFEFEFEFEFEFEFEFEL;
    private static final long DOWN = 0xFFFFFFFFFFFFFF00L;
    private static final long UP = 0x00FFFFFFFFFFFFFFL;


    //based on "shift" and "shiftable" of https://github.com/clarkkev/othello-ai/blob/master/src/othellosaurus/Utils.java
    //right / left = 1
    //down / up = 8
    //down right / up left = 9
    //down left / up right = 7
    private static final int[] SHIFTS = {1, 8, 9, 7};
    public static final long[][] MASKS = {
            {RIGHT, LEFT},
            {DOWN, UP},
            {DOWN & RIGHT, UP & LEFT},
            {DOWN & LEFT, UP & RIGHT}
    };

    private static final int[] CORNER_SHIFTS = {0, BOARD_SIZE - 1, BOARD_SIZE * (BOARD_SIZE - 1), BOARD_SIZE * BOARD_SIZE};
    private static final int[][] CORNER_ADJACENT_SHIFTS = {
            {1, BOARD_SIZE, BOARD_SIZE + 1},
            {BOARD_SIZE - 2, 2 * BOARD_SIZE - 2, 2 * BOARD_SIZE - 1},
            {BOARD_SIZE * BOARD_SIZE - 2 * BOARD_SIZE,
                    BOARD_SIZE * BOARD_SIZE - 2 * BOARD_SIZE + 1, BOARD_SIZE * (BOARD_SIZE - 1) + 1},
            {BOARD_SIZE * (BOARD_SIZE - 1) - 2,
                    BOARD_SIZE * (BOARD_SIZE - 1) - 1, BOARD_SIZE * BOARD_SIZE - 2}
    };

    private Board() {}

    public static void initBitBoards(Player player1, Player player2) {
        player1.setStones(initBitBoard(-1, 1));
        player2.setStones(initBitBoard(1, 0));
    }

    private static long initBitBoard(int a, int b) {
        int start = (BOARD_SIZE - 2) / 2;
        long l = 1L;

        //move by distance between the starting bitBoards
        l <<= BOARD_SIZE + a;

        l |= 1L;

        //move to initial position
        l <<= start + b;

        //create empty fields
        for (int i = 0; i < start; i++) {
            l <<= BOARD_SIZE;
        }
        return l;
    }

    // based on this move calculation: https://gist.github.com/davidrobles/4042418 18.11.2016
    public static long calculatePossibleTurns(long current, long opponent) {
        long empty = ~(current | opponent);
        long possibleTurns = 0L;

        for (int direction = 0; direction < SHIFTS.length; direction++) {
            int shift = SHIFTS[direction];

            for (int leftOrRightShift = 0; leftOrRightShift < 2; leftOrRightShift++) {
                long mask = MASKS[direction][leftOrRightShift];

                long potentialTurns = (leftOrRightShift == 0) ?
                        (current << shift & mask & opponent) :
                        (current >>> shift & mask & opponent);

                while (potentialTurns != 0L) {
                    long move = (leftOrRightShift == 0) ?
                            (potentialTurns << shift & mask) :
                            (potentialTurns >>> shift & mask);
                    possibleTurns |= move & empty;
                    potentialTurns = move & opponent;
                }
            }
        }

        return possibleTurns;
    }

    public static long calculateAndValidateTurn(long turns, int x, int y) {
        int shift = y * BOARD_SIZE + x;

        if (shift >= 0 && shift < BOARD_SIZE * BOARD_SIZE) {
            long turn = 1L << (shift);

            if ((turn & turns) != 0) {
                return turn;
            }
        }
        return 0L;
    }

    public static Pair<Long, Long> flipStones(long current, long opponent, long turn) {
        long flippableStones = findFlippableStones(current, opponent, turn);
        return new Pair<>(current | flippableStones | turn, opponent ^ flippableStones);
    }

    private static long findFlippableStones(long current, long opponent, long turn) {
        // based on this flip calculation: https://gist.github.com/davidrobles/4042418 18.11.2016
        long flippableStones = 0L;

        for (int direction = 0; direction < SHIFTS.length; direction++) {
            int shift = SHIFTS[direction];

            for (int leftOrRightShift = 0; leftOrRightShift < 2; leftOrRightShift++) {
                long mask = MASKS[direction][leftOrRightShift];

                long lastStone = 0L;
                long potentialFlips = 0L;

                long potentialFlip = (leftOrRightShift == 0) ?
                        (turn << shift & mask & opponent) :
                        (turn >>> shift & mask & opponent);

                while (potentialFlip != 0L) {
                    potentialFlips |= potentialFlip;
                    long move = (leftOrRightShift == 0) ?
                            (potentialFlip << shift & mask) :
                            (potentialFlip >>> shift & mask);
                    lastStone = move & current;
                    potentialFlip = move & opponent;
                }

                if (lastStone == 0L) {
                    potentialFlips = 0L;
                }

                flippableStones |= potentialFlips;
            }
        }
        return flippableStones;
    }


    // https://kartikkukreja.wordpress.com/2013/03/30/heuristic-function-for-reversiothello/ 20.11.2016
    //http://mkorman.org/othello.pdf
    public static double calculateBoardValue(long current, long opponent) {
        long currentTurns = calculatePossibleTurns(current, opponent);
        long opponentTurns = calculatePossibleTurns(opponent, current);

        double stoneParity = calculateStoneParity(current, opponent);
        double capturedCorners = calculateCapturedCorners(current, opponent);
        double closeToEmptyCorners = calculateCornerCloseness(current, opponent);
        double mobility = calculateMobility(currentTurns, opponentTurns);
        double potentialFlippableStones = calculatePotentialFlippables(current, opponent, currentTurns, opponentTurns);

        return (10 * stoneParity) +
                (801.724 * capturedCorners) +
                (382.026 * closeToEmptyCorners) +
                (78.922 * mobility) +
                (74.396 * potentialFlippableStones);
    }

    private static double calculateStoneParity(long current, long opponent) {
        int numberOfPlayerStones = countSetBits(current);
        int numberOfOpponentStones = countSetBits(opponent);
        double stoneParity = 0;

        if (numberOfPlayerStones > numberOfOpponentStones) {
            stoneParity =
                    100 * (numberOfPlayerStones) / (numberOfPlayerStones + numberOfOpponentStones);
        }

        else if (numberOfOpponentStones > numberOfPlayerStones) {
            stoneParity =
                    -100 * (numberOfOpponentStones) / (numberOfPlayerStones + numberOfOpponentStones);
        }

        return stoneParity;
    }

    private static double calculateCapturedCorners(long current, long opponent) {
        int numberOfPlayerCorners = countFields(current, CORNER_SHIFTS);
        int numberOfOpponentCorners = countFields(opponent, CORNER_SHIFTS);
        return 25 * numberOfPlayerCorners - 25 * numberOfOpponentCorners;
    }

    private static double calculateCornerCloseness(long current, long opponent) {
        double numberOfPlayerStonesCloseToEmptyCorners = 0;
        for (int corner = 0; corner < CORNER_SHIFTS.length; corner++) {
            if (!isBitSet(current, CORNER_SHIFTS[corner])) {
                numberOfPlayerStonesCloseToEmptyCorners +=
                        countFields(current, CORNER_ADJACENT_SHIFTS[corner]);
            }
        }

        double numberOfOpponentStonesCloseToEmptyCorners = 0;
        for (int corner = 0; corner < CORNER_SHIFTS.length; corner++) {
            if (!isBitSet(opponent, CORNER_SHIFTS[corner])) {
                numberOfOpponentStonesCloseToEmptyCorners +=
                        countFields(opponent, CORNER_ADJACENT_SHIFTS[corner]);
            }
        }

        return  -12.5 * numberOfPlayerStonesCloseToEmptyCorners +
                12.5 * numberOfOpponentStonesCloseToEmptyCorners;
    }

    private static double calculateMobility(long currentTurns, long opponentTurns) {
        int numberOfPlayerTurns = countSetBits(currentTurns);
        int numberOfOpponentTurns = countSetBits(opponentTurns);
        double mobility = 0;

        if (numberOfPlayerTurns + numberOfOpponentTurns != 0) {
            if (numberOfPlayerTurns > numberOfOpponentTurns) {
                mobility =
                        100 * (numberOfPlayerTurns) / (numberOfPlayerTurns + numberOfOpponentTurns);
            }

            else if (numberOfOpponentTurns > numberOfPlayerTurns){
                mobility =
                        -100 * (numberOfOpponentTurns) / (numberOfPlayerTurns + numberOfOpponentTurns);
            }
        }

        return mobility;
    }

    private static double calculatePotentialFlippables(long current, long opponent, long currentTurns, long opponentTurns) {
        int numberOfPotentialFlippablePlayerStones = countSetBits(findFlippableStones(current, opponent, currentTurns));
        int numberOfPotentialFlippableOpponentStones = countSetBits(findFlippableStones(opponent, current, opponentTurns));
        double potentialFlippableStones = 0;

        if (numberOfPotentialFlippablePlayerStones + numberOfPotentialFlippableOpponentStones != 0) {
            if (numberOfPotentialFlippablePlayerStones > numberOfPotentialFlippableOpponentStones) {
                potentialFlippableStones =
                        100 * (numberOfPotentialFlippablePlayerStones) /
                                (numberOfPotentialFlippablePlayerStones + numberOfPotentialFlippableOpponentStones);
            }

            else if (numberOfPotentialFlippableOpponentStones > numberOfPotentialFlippablePlayerStones) {
                potentialFlippableStones =
                        -100 * (numberOfPotentialFlippableOpponentStones) /
                                (numberOfPotentialFlippablePlayerStones + numberOfPotentialFlippableOpponentStones);
            }
        }

        return potentialFlippableStones;
    }

    public static int countSetBits(long bitBoard) {
        int counter = 0;

        while (bitBoard != 0L) {
            counter++;
            bitBoard &= (bitBoard - 1);
        }

        return counter;
    }

    private static int countFields(long bitBoard, int[] shifts) {
        int counter = 0;

        for (int shift : shifts) {
            if ((bitBoard & (1L << shift)) != 0) {
                counter++;
            }
        }
        return counter;
    }

    private static boolean isBitSet(long bitBoard, int shift) {
        return (bitBoard & (1L << shift)) != 0;
    }

    public static int getBoardSize() {
        return BOARD_SIZE;
    }
}
