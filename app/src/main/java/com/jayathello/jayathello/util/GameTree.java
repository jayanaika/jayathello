package com.jayathello.jayathello.util;

import android.util.Pair;

import com.jayathello.jayathello.entity.Player;


/**
 * Created by Julian on 31.10.2016.
 */

public class GameTree {
    private static int depth;
    private static long bestTurn;

    private GameTree() {}

    public static long getTurn(Player player, int depth) {
        GameTree.depth = depth;
        bestTurn = 0L;
        createTreeAndEvaluate(player.getStones(), player.getOpponent().getStones(), depth, Double.MIN_VALUE, Double.MAX_VALUE);
        return bestTurn;
    }

    private static double createTreeAndEvaluate(long current, long opponent, int depth, double alpha, double beta) {
        if (depth == 0) {
            return Board.calculateBoardValue(current, opponent);
        }

        long turns = Board.calculatePossibleTurns(current, opponent);

        if (depth == GameTree.depth) {
            bestTurn = turns ^ (turns & (turns - 1));
        }

        while (turns != 0L) {
            long turn = turns ^ (turns & (turns - 1)); //Get LSB that is 1

            Pair<Long, Long> boardPair =  Board.flipStones(current, opponent, turn);

            double boardValue = -createTreeAndEvaluate(boardPair.second, boardPair.first, depth - 1, -beta, -alpha);

            if (boardValue > alpha) {
                alpha = boardValue;

                if (depth == GameTree.depth) {
                    bestTurn = turn;
                }

                if (alpha >= beta) {
                    return alpha;
                }
            }

            turns &= turns - 1; //Remove LSB that is 1
        }
        return alpha;
    }
}