package com.jayathello.jayathello.views;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.canvas.StoneSymbol;
import com.jayathello.jayathello.controllers.interfaces.GameResultInterface;
import com.jayathello.jayathello.entity.Player;
import com.jayathello.jayathello.enums.GameType;
import com.jayathello.jayathello.enums.PlayerType;
import com.jayathello.jayathello.util.Board;

/**
 * Created by julian on 02.02.17.
 */

public class GameResultView extends JayaView<GameResultInterface> implements View.OnClickListener {

    private TextView resultText;
    private StoneSymbol stoneSymbolPlayer1;
    private StoneSymbol stoneSymbolPlayer2;
    private Button rematchButton;
    private Button backToMenuButton;

    public GameResultView(View view, GameType gameType, Player player1, Player player2) {
        super(view);

        int numberOfStonesPlayer1 = Board.countSetBits(player1.getStones());
        int numberOfStonesPlayer2 = Board.countSetBits(player2.getStones());

        resultText = (TextView) view.findViewById(R.id.result);

        if (gameType == GameType.SINGLEPLAYER) {
            String result = view.getResources().getString(R.string.lostGame);
            if (player1.getType() == PlayerType.PLAYER) {
                if (numberOfStonesPlayer1 > numberOfStonesPlayer2) {
                    result = view.getResources().getString(R.string.wonGame);
                }
            }

            else {
                if (numberOfStonesPlayer2 > numberOfStonesPlayer1) {
                    result = view.getResources().getString(R.string.wonGame);
                }
            }
            resultText.setText(result);
        }

        if (numberOfStonesPlayer1 == numberOfStonesPlayer2) {
            resultText.setText(view.getResources().getString(R.string.drawGame));
        }

        stoneSymbolPlayer1 = (StoneSymbol) view.findViewById(R.id.stoneSymbolPlayer1);
        stoneSymbolPlayer1.init(player1.getStoneType(), String.valueOf(numberOfStonesPlayer1));

        stoneSymbolPlayer2 = (StoneSymbol) view.findViewById(R.id.stoneSymbolPlayer2);
        stoneSymbolPlayer2.init(player2.getStoneType(), String.valueOf(numberOfStonesPlayer2));

        rematchButton = (Button) view.findViewById(R.id.rematchButton);
        backToMenuButton = (Button) view.findViewById(R.id.backToMenuButton);

        rematchButton.setOnClickListener(this);
        backToMenuButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rematchButton: {
                controller.rematch();
            } break;

            case R.id.backToMenuButton: {
                controller.backToMenu();
            } break;
        }
    }
}
