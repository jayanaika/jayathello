package com.jayathello.jayathello.views;

import android.view.MotionEvent;
import android.view.View;

import com.jayathello.jayathello.entity.Player;
import com.jayathello.jayathello.R;
import com.jayathello.jayathello.canvas.GameBoard;
import com.jayathello.jayathello.canvas.StoneSymbol;
import com.jayathello.jayathello.controllers.interfaces.GameInterface;

import java.util.Map;


/**
 * Created by Julian on 04.02.2017.
 */


public class GameView extends JayaView<GameInterface> implements View.OnTouchListener {

    private GameBoard gameBoard;
    private StoneSymbol stoneSymbolPlayer1;
    private StoneSymbol stoneSymbolPlayer2;

    public GameView(View view, Player currentPlayer) {
        super(view);

        gameBoard = (GameBoard) view.findViewById(R.id.gameBoard);
        gameBoard.setOnTouchListener(this);
        gameBoard.init(currentPlayer);

        stoneSymbolPlayer1 = (StoneSymbol) view.findViewById(R.id.stoneSymbolPlayer1);
        stoneSymbolPlayer1.init(currentPlayer.getStoneType(), "2");

        stoneSymbolPlayer2 = (StoneSymbol) view.findViewById(R.id.stoneSymbolPlayer2);
        stoneSymbolPlayer2.init(currentPlayer.getOpponent().getStoneType(), "2");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.gameBoard: {
                return gameBoardTouched(event);
            }
            default: return false;
        }
    }

    private boolean gameBoardTouched(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                float squareSize = gameBoard.getSquareSize();

                int x = (int)(event.getX() / squareSize);
                int y = (int)(event.getY() / squareSize);

                controller.onTouchDown(x, y);
                return true;
            }
            default: return false;
        }
    }

    public void updateGameBoard(Player currentPlayer) {
        gameBoard.render(currentPlayer);
    }

    public void updateNumberOfStones(int amountP1, int amountP2) {
        stoneSymbolPlayer1.setText(String.valueOf(amountP1));
        stoneSymbolPlayer2.setText(String.valueOf(amountP2));
    }
}

