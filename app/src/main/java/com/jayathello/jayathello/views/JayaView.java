package com.jayathello.jayathello.views;

import android.view.View;
import android.widget.Toast;

import com.jayathello.jayathello.controllers.JayaController;
import com.jayathello.jayathello.controllers.interfaces.JayaControllerInterface;

/**
 * Created by julian on 03.02.17.
 */

public abstract class JayaView<I extends JayaControllerInterface> {
    protected View view;
    protected I controller;

    protected JayaView(View view) {
        this.view = view;
    }

    public void setController(I controller) {
        this.controller = controller;
    }
}
