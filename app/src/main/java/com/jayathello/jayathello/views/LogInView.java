package com.jayathello.jayathello.views;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.interfaces.LogInInterface;

/**
 * Created by julian on 25.01.17.
 */

public class LogInView extends JayaView<LogInInterface> implements View.OnClickListener, TextView.OnEditorActionListener {
    private TextInputEditText emailEditText;
    private TextInputEditText passwordEditText;
    private TextInputLayout emailTextInputLayout;
    private TextInputLayout passwordTextInputLayout;
    private Button loginButton;
    private Button signUpButton;

    public LogInView(View view) {
        super(view);

        loginButton = (Button)  view.findViewById(R.id.loginButton);
        signUpButton = (Button) view.findViewById(R.id.signUpButton);
        emailEditText = (TextInputEditText) view.findViewById(R.id.emailEditText);
        passwordEditText = (TextInputEditText) view.findViewById(R.id.passwordEditText);
        emailTextInputLayout = (TextInputLayout) view.findViewById(R.id.emailTextInputLayout);
        passwordTextInputLayout = (TextInputLayout) view.findViewById(R.id.passwordTextInputLayout);

        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        emailEditText.setOnEditorActionListener(this);
        passwordEditText.setOnEditorActionListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton: {
                controller.logIn(
                        emailEditText.getText().toString(), passwordEditText.getText().toString());
            } break;

            case R.id.signUpButton: {
                controller.signUp();
            } break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (v.getId()) {
            case R.id.emailEditText: {
                controller.isValidEmail(emailEditText.getText().toString());
            } break;

            case R.id.passwordEditText: {
                controller.isValidPassword(passwordEditText.getText().toString());
            } break;
        }
        return false;
    }

    public void setEmailError(String text) {
        emailTextInputLayout.setError(text);
    }

    public void setPasswordError(String text) {
        passwordTextInputLayout.setError(text);
    }

    public void clearTextInput() {
        emailEditText.setText("");
        passwordEditText.setText("");
    }
}
