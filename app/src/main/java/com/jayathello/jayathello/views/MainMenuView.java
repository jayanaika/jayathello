package com.jayathello.jayathello.views;

import android.view.View;
import android.widget.Button;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.interfaces.MainMenuInterface;

/**
 * Created by julian on 29.01.17.
 */

public class MainMenuView extends JayaView<MainMenuInterface> implements View.OnClickListener {
    private Button singleplayerButton;
    private Button multiplayerButton;

    public MainMenuView(View view) {
        super(view);

        singleplayerButton = (Button) view.findViewById(R.id.singleplayerButton);
        multiplayerButton = (Button) view.findViewById(R.id.multiplayerButton);

        singleplayerButton.setOnClickListener(this);
        multiplayerButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.singleplayerButton: {
                controller.singlePlayer();
            } break;

            case R.id.multiplayerButton: {
                controller.multiPlayer();
            } break;
        }
    }
}
