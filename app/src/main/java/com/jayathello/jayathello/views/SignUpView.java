package com.jayathello.jayathello.views;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.controllers.interfaces.SignUpInterface;

/**
 * Created by Julian on 11.02.2017.
 */

public class SignUpView extends JayaView<SignUpInterface> implements View.OnClickListener, TextView.OnEditorActionListener {
    private TextInputEditText emailEditText;
    private TextInputEditText passwordEditText;
    private TextInputEditText confirmPasswordEditText;

    private TextInputLayout emailTextInputLayout;
    private TextInputLayout passwordTextInputLayout;
    private TextInputLayout confirmPasswordTextInputLayout;

    private Button signUpButton;

    public SignUpView(View view) {
        super(view);

        signUpButton = (Button) view.findViewById(R.id.signUpButton);
        emailEditText = (TextInputEditText) view.findViewById(R.id.emailEditText);
        passwordEditText = (TextInputEditText) view.findViewById(R.id.passwordEditText);
        confirmPasswordEditText = (TextInputEditText) view.findViewById(R.id.confirmPasswordEditText);

        emailTextInputLayout = (TextInputLayout) view.findViewById(R.id.emailTextInputLayout);
        passwordTextInputLayout = (TextInputLayout) view.findViewById(R.id.passwordTextInputLayout);
        confirmPasswordTextInputLayout = (TextInputLayout) view.findViewById(R.id.confirmPasswordTextInputLayout);

        signUpButton.setOnClickListener(this);
        emailEditText.setOnEditorActionListener(this);
        passwordEditText.setOnEditorActionListener(this);
        confirmPasswordEditText.setOnEditorActionListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpButton: {
                controller.signUp(
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString(),
                        confirmPasswordEditText.getText().toString());
            } break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (v.getId()) {
            case R.id.emailEditText: {
                if(controller.isEmail(emailEditText.getText().toString())) {
                    controller.isEmailAvailable(emailEditText.getText().toString());
                }
            } break;

            case R.id.passwordEditText: {
                controller.isPasswordLongEnough(passwordEditText.getText().toString());
            } break;

            case R.id.confirmPasswordEditText: {
                controller.isPasswordSame(
                        passwordEditText.getText().toString(),
                        confirmPasswordEditText.getText().toString());
            } break;
        }
        return false;
    }

    public void setEmailError(String text) {
        emailTextInputLayout.setError(text);
    }

    public void setPasswordError(String text) {
        passwordTextInputLayout.setError(text);
    }

    public void setConfirmPasswordError(String text) {
        confirmPasswordTextInputLayout.setError(text);
    }
}
