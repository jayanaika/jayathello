package com.jayathello.jayathello.views;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.jayathello.jayathello.R;
import com.jayathello.jayathello.canvas.StoneSymbol;
import com.jayathello.jayathello.controllers.interfaces.SinglePlayerOptionsInterface;
import com.jayathello.jayathello.enums.DifficultyType;
import com.jayathello.jayathello.enums.StoneType;

/**
 * Created by Julian on 25.02.2017.
 */

public class SinglePlayerOptionsView extends JayaView<SinglePlayerOptionsInterface> implements View.OnTouchListener, View.OnClickListener {

    private static final String SELECTED_STONE_SYMBOL_SYMBOL = "X";

    private RadioGroup difficultyRadioGroup;
    private StoneSymbol stoneSymbolPlayer1;
    private StoneSymbol stoneSymbolPlayer2;
    private Button startGameButton;

    public SinglePlayerOptionsView(View view) {
        super(view);

        difficultyRadioGroup = (RadioGroup) view.findViewById(R.id.difficultyRadioGroup);

        stoneSymbolPlayer1 = (StoneSymbol) view.findViewById(R.id.stoneSymbolPlayer1);
        stoneSymbolPlayer1.init(StoneType.BLACK, SELECTED_STONE_SYMBOL_SYMBOL);
        stoneSymbolPlayer1.setOnTouchListener(this);

        stoneSymbolPlayer2 = (StoneSymbol) view.findViewById(R.id.stoneSymbolPlayer2);
        stoneSymbolPlayer2.init(StoneType.WHITE);
        stoneSymbolPlayer2.setOnTouchListener(this);

        startGameButton = (Button) view.findViewById(R.id.startGameButton);
        startGameButton.setOnClickListener(this);
    }

    public DifficultyType getDifficultyType() {
        switch (difficultyRadioGroup.getCheckedRadioButtonId()) {
            case R.id.difficultyEasy: {
                return DifficultyType.EASY;
            }

            case R.id.difficultyMedium: {
                return DifficultyType.MEDIUM;
            }

            case R.id.difficultyHard: {
                return DifficultyType.HARD;
            }

            default: return null;
        }
    }

    public StoneType getStoneType() {
        if (stoneSymbolPlayer1.getText().equals(SELECTED_STONE_SYMBOL_SYMBOL)) {
            return stoneSymbolPlayer1.getStoneType();
        }

        if (stoneSymbolPlayer2.getText().equals(SELECTED_STONE_SYMBOL_SYMBOL)) {
            return stoneSymbolPlayer2.getStoneType();
        }

        return null;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.stoneSymbolPlayer1: {
                stoneSymbolPlayer1.setText(SELECTED_STONE_SYMBOL_SYMBOL);
                stoneSymbolPlayer2.setText("");
                return true;
            }

            case R.id.stoneSymbolPlayer2: {
                stoneSymbolPlayer2.setText(SELECTED_STONE_SYMBOL_SYMBOL);
                stoneSymbolPlayer1.setText("");
                return true;
            }

            default: return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startGameButton: {
                controller.startGame(getDifficultyType(), getStoneType());
            }
        }
    }
}
